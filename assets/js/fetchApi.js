
// Functions for communicating with backend-API here...


function postOrder(order) {
    return fetch(
        `${API_URL}/orders`, 
        {
            method: 'POST', 
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(order)
        }
    );
}


function fetchSomething() {
    return fetch(
        `${API_URL}/something`,
        {
            method: 'GET'
        }
    )
    .then(something => something.json());
}

function postSomething(something) {
    return fetch(
        `${API_URL}/something`, 
        {
            method: 'POST', 
            body: JSON.stringify(something)
        }
    );
}

function deleteSomething(id) {
    return fetch(
        `${API_URL}/something/${id}`,
        {
            method: 'DELETE'
        }
    );
}
