var toLangPrice = 0;
var serviceCoeficient = 0;
var qualityLevel = 0;
var pageCount = 0;
var resultPrice = 0;

 function loadFromLanguages() {
    let fromLanguagesSelectElement = document.getElementById("fromLanguages");

    let option = document.createElement('option');
    option.text = "Choose language";
    fromLanguagesSelectElement.add(option);

    for (let i = 0; i < translations.length; i++) {
        let option = document.createElement('option');
        option.text = translations[i].name;
        option.value = translations[i].name;
        fromLanguagesSelectElement.add(option);
    }
}

function handleFromLanguageChange() {
    let fromLanguagesSelectElement = document.getElementById("fromLanguages");
    let targetLanguagesSelectElement = document.getElementById("targetLanguages");

      
        let selectedLanguageObject = getSelectedFromLanguageObject(fromLanguagesSelectElement.value);
        console.log(selectedLanguageObject);

        let targetLangs = selectedLanguageObject.toLangs;
        for(let i = 0; i < targetLangs.length; i++) {
            let option = document.createElement('option');
            option.text = targetLangs[i].name;
            option.value = targetLangs[i].price;
            targetLanguagesSelectElement.add(option);
        }
    
            }

    

   
function getSelectedFromLanguageObject(fromLanguageName){
    for(let i = 0; i < translations.length; i++) {
        if (translations[i].name == fromLanguageName) {
            return translations[i];
        }
    }
    return null;
}



function handleServiceTypeChange() {
    let selectedType = document.getElementById("serviceType").value;
    let selectedTypeObject = getSelectedFromLanguageObject(selectedType.value);
    console.log(selectedTypeObject);

    let targetLanguagesSelectElement = document.getElementById("serviceType");
    let targetLangs = selectedTypeObject.toLangs;

for(let i = 0; i < targetLangs.length; i++) {
    let option = document.createElement('option');
    option.text = targetLangs[i].name;
    option.value = targetLangs[i].price;
    targetLanguagesSelectElement.add(option);
}

}

function calculatePageCount() {
    //    console.log("ok");
    let number1Value=document.getElementById("number1").value;
    let number2Value=document.getElementById("number2").value;

    if (number1Value > 0) {
        pageCount = parseInt(number1Value) / 250;
    //    console.log(pageCount);
    }
    else if (number2Value > 0) {
        pageCount = parseInt(number2Value) / 1800;
    
        
    }

    pageCount = pageCount.toFixed(0);
    if (((number1Value > 0) || (number2Value > 0)) && (pageCount == 0)) {
        pageCount = 1;
    }

    document.getElementById("pageCount").innerText=pageCount;
    //calculatePrice();
}


 
  
function calculatePrice() { 

    let targetLanguageValue = document.getElementById("targetLanguages").value;
    let serviceType = document.getElementById("serviceType").value;
    let quality = document.getElementById("quality").value;
    let fileFormat = document.getElementById("format").value;

    console.log(targetLanguageValue, serviceType, quality, fileFormat);
    let pageCount = parseInt(document.getElementById("pageCount").innerText);
    

    resultPrice = parseFloat(targetLanguageValue) * parseFloat(serviceType) * parseFloat(quality) * parseFloat(fileFormat) * pageCount;
    resultPrice = resultPrice.toFixed(2);
    document.getElementById("price").innerText = resultPrice;

}

function sendOrder() {
    let fullName = document.getElementById("fullName").value;
    let eMail = document.getElementById("eMail").value;
    let phoneNumber = document.getElementById("phoneNumber").value;
    let communicationLanguage = document.getElementById("commLanguage").value;
    let typeOfService = document.getElementById("serviceType").value;
    let levelOfQuality = document.getElementById("quality").value;
    let formatSelect = document.getElementById("format").value
    let formatExtension = formatSelect.options[formatSelect.selectedIndex].text; 
    let numberOfPages = pageCount;
    let orderingPrice = resultPrice;
    document.getElementById('format')



    let order = {
        customer_name: fullName,
        customer_mail: eMail, 
        customer_phone: phoneNumber,
        customer_lang: communicationLanguage,
        service_type: typeOfService,
        quality_level:levelOfQuality,
        file_format:formatSelect,
        format_extension:formatExtension,
        page_count:numberOfPages,
        order_price:orderingPrice

    };

    postOrder(order).then(
        function () {
            alert("Order sent successfully!");
        }
    );
}
