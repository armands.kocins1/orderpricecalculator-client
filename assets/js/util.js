var translations = [
    {
        name: "Danish",
        toLangs: [
            {
                name: "English",
                price: 10.00
            },
            {
                name: "Estonian",
                price: 8.00
            },
            
              
        ]
    },
    {
        name: "Estonian",
        toLangs: [
            {
                name: "Danish",
                price: 9.00
            },
            {
                name: "English",
                price: 8.00
            },
            {
                name: "Finnish",
                price: 9.00
            },
            {
                name: "German",
                price: 8.00
            },
            {
                name: "Latgalian",
                price: 9.00
            },
            {
                name: "Latvian",
                price: 9.0
            },
            {
                name: "Lithuanian",
                price: 0.0077
            },
            {
                name: "Norwegian",
                price: 0.1
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.1
            },            
            {
                name: "Polish",
                price: 0.0077
            },            
            {
                name: "Russian",
                price: 0.0044
            },            
            {
                name: "Samogitian",
                price: 0.1
            },            
            {
                name: "Swedish",
                price: 0.1
            },            
            {
                name: "Ukrainian",
                price: 0.0077
            }
              
        ]
    },
    {
        name: "Latgalian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 11.000
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },            
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Latvian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },            
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Lithuanian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },            
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Norwegian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Norwegian (Bokmål)",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Norwegian (Nynorsk)",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },            
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Polish",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },    
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Russian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Polish",
                price: 0.005
            },            
                       
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Samogitian",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },    
            {
                name: "Polish",
                price: 0.005
            },
            {            
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Swedish",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },    
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
        {
        name: "Polish",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },    
            {
                name: "Polish",
                price: 0.005
            },            
            {
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },            
            {
                name: "Swedish",
                price: 0.005
            },            
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    },
    {
        name: "Swedish",
        toLangs: [
            {
                name: "Danish",
                price: 0.005
            },
            {
                name: "English",
                price: 0.005
            },
            {
                name: "Estonian",
                price: 0.005
            },
            {
                name: "Finnish",
                price: 0.005
            },
            {
                name: "German",
                price: 0.005
            },
            {
                name: "Latgalian",
                price: 0.005
            },
            {
                name: "Latvian",
                price: 0.005
            },
            {
                name: "Lithuanian",
                price: 0.005
            },
            {
                name: "Norwegian",
                price: 0.005
            },
            {
                name: "Norwegian (Bokmål)",
                price: 0.005
            },  
            {
                name: "Norwegian (Nynorsk)",
                price: 0.005
            },    
            {
                name: "Polish",
                price: 0.005
            },
            {            
                name: "Russian",
                price: 0.005
            },            
            {
                name: "Samogitian",
                price: 0.005
            },           
            {
                name: "Ukrainian",
                price: 0.005
            }
        ]
    }
];